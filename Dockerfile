FROM nginx

RUN mkdir -p /workspace/from-gitlab/build

WORKDIR /workspace/from-gitlab

COPY build /workspace/from-gitlab/

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/nginx.conf

COPY . .




EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]
